import React from 'react';
import {BrowserRouter as Router, Route, Link} from "react-router-dom";
import 'bootstrap/dist/css/bootstrap.css'
import CreateTodo from "./components/create-todo.component";
import TodoList from "./components/todolist.component";
import EditTodo from "./components/edit-todo.component";

class App extends React.Component {
    render() {
        return (
            <Router>
                <div className="container">

                    <nav className="navbar navbar-expand-lg navbar-light bg-light">
                        <Link to="/" className = "navbar-brand">MERN TODO App</Link>

                        <div className="collapse navbar-collapse">
                            <ul className="navbar-nav mr-auto">
                                <li className="nav-item">
                                    <Link to="/" className = "nav-link">Todos</Link>
                                </li>
                                <li className="navbar-item">
                                    <Link to="/create" className = "nav-link">Create todo</Link>
                                </li>
                            </ul>
                        </div>


                    </nav>

                    <Route path="/" exact component={TodoList}/>
                    <Route path="/edit/:id" component={EditTodo}/>
                    <Route path="/create" component={CreateTodo}/>
                </div>


            </Router>

        )
    }
}

export default App;
